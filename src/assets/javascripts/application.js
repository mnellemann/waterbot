//= require jquery/jquery.js
//= require intercooler/intercooler.js
//= require fontawesome/js/all.js
//= require_tree .
//= require_self


function updateValve(valve_id) {

    var valve_name  = document.getElementById(valve_id+"_name").value;
    var valve_quota = document.getElementById(valve_id+"_quota").value;

    console.log("Valve Name:  " + valve_name);
    console.log("Valve Quota: " + valve_quota);

    var payload = {id: valve_id, name: valve_name, quota: valve_quota};
    var request =
    {
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(payload)
    };

    return fetch('/valve', request)
        .then(response =>
            {
                if (response.ok) {
                    console.log('Success:', JSON.stringify(response))
                }
                if (response.ok == false) { //response not ok
                    alert("Cannot update value. The name is not unique or the quote is out of range (0,10>")
                }
            })
        .catch(error => console.error(error));
}
