package waterbot

import groovy.transform.CompileStatic

@CompileStatic
enum RaspberryOutput {

    output1 ("Valve 1"),
    output2 ("Valve 2"),
    output3 ("Valve 3"),
    output4 ("Valve 4"),
    output5 ("Valve 5"),
    output6 ("Valve 6"),
    output7 ("Valve 7");

    private final name;

    private RaspberryOutput(String s) {
        name = s;
    }

    boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return name.equals(otherName);
    }

    String toString() {
        return this.name;
    }
}
