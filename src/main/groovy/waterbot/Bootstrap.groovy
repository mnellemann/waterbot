package waterbot

import groovy.util.logging.Slf4j
import org.grails.datastore.mapping.validation.ValidationException
import waterbot.domain.Valve
import waterbot.services.ValveDataService
import io.micronaut.context.event.StartupEvent
import io.micronaut.runtime.event.annotation.EventListener

import javax.inject.Inject
import javax.inject.Singleton

@Slf4j
@Singleton
class Bootstrap {

    @Inject
    ValveDataService valveDataService

    Bootstrap(ValveDataService valveService) {
        this.valveDataService = valveService
    }


    @EventListener
    void onStartup(StartupEvent event) {


        try {
            valveDataService.save(new Valve(name: 'Jordbær', quota: 1, output: RaspberryOutput.output1))
            valveDataService.save(new Valve(name: 'Citron', quota: 2, output: RaspberryOutput.output2))
            valveDataService.save(new Valve(name: 'Tomater', quota: 3, output: RaspberryOutput.output3))
            valveDataService.save(new Valve(name: 'Meloner', quota: 4, output: RaspberryOutput.output4))
            valveDataService.save(new Valve(name: 'Hindbær', quota: 5, output: RaspberryOutput.output5))
            valveDataService.save(new Valve(name: 'Hampen', quota: 6, output: RaspberryOutput.output6))
            valveDataService.save(new Valve(name: 'Skunken', quota: 6, output: RaspberryOutput.output7))
        } catch(ValidationException ex) {
            log.warn("not creating bootstrap data")
        }

    }

}
