package waterbot.services

import groovy.util.logging.Slf4j
import groovy.transform.CompileStatic
import waterbot.domain.Valve

import javax.inject.Singleton
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

@Slf4j
@Singleton
@CompileStatic
class QueueService {

    private static BlockingQueue<Valve> blockingQueue = new LinkedBlockingQueue<Valve>();

    int count() {
        return blockingQueue.size()
    }

    void offer(Valve valve) {
        if(!blockingQueue.contains(valve)) {
            blockingQueue.offer(valve)
        }
    }

    Valve poll() {
        blockingQueue.poll()
    }

    List<Valve> list() {

        List<Valve> valves = []
        Object[] list = blockingQueue.toArray();
        for (Object i : list) {
            valves.add((Valve)i)
        }

        return valves
    }

}
