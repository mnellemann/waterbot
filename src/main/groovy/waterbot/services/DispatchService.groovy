package waterbot.services

import grails.gorm.transactions.Transactional
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import waterbot.domain.Valve

import javax.inject.Inject

@Slf4j
@Transactional
@CompileStatic
class DispatchService {

    @Inject
    QueueService queueDataService

    @Inject
    RaspberryService raspberryService

    def processQueue() {

        Valve valve
        while(valve = queueDataService.poll()) {
            raspberryService.doWater(valve)
        }

        /*
        List<Queue> queueList = queueDataService.list()
        queueList.each { Queue queue ->

            log.info("processQueue() - processing item from queue for ${queue.valve.name}")
            try{
                raspberryService.doWater(queue.valve)
                queueDataService.delete(queue.id)
            } catch(Exception ex) {
                log.error("processQueue() - something went wrong", ex)
            }
        }
        */
    }


}
