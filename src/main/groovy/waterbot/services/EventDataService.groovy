package waterbot.services

import waterbot.domain.Event
import grails.gorm.services.Service
import groovy.transform.CompileStatic

@Service(Event)
@CompileStatic
interface EventDataService {

    Event get(Serializable id)

    List<Event> list(Map args)

    Long count()

    Event save(Event event)
}
