package waterbot.services

import waterbot.domain.Valve
import grails.gorm.services.Service
import groovy.transform.CompileStatic

@Service(Valve)
@CompileStatic
interface ValveDataService {

    Valve get(Serializable id)

    List<Valve> list()
    List<Valve> list(Map args)

    Long count()

    Valve save(Valve valve)
}
