package waterbot.services

import io.micronaut.runtime.event.annotation.EventListener
import io.micronaut.discovery.event.ServiceShutdownEvent
import waterbot.domain.Event
import waterbot.domain.Valve

import javax.inject.Inject
import javax.inject.Singleton
import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.util.logging.Slf4j
import com.pi4j.system.SystemInfo;
import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.GpioPinDigitalInput
import com.pi4j.io.gpio.PinPullResistance
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.io.gpio.event.GpioPinDigitalStateChangeEvent;
import com.pi4j.io.gpio.event.GpioPinListenerDigital;
import waterbot.RaspberryOutput;

@Slf4j
@Singleton
@CompileStatic
class RaspberryService {

    @Inject
    ValveDataService valveDataService

    @Inject
    EventDataService eventDataService

    // GPIO controller
    private final GpioController gpio

    // Power LED
    private final GpioPinDigitalOutput powerLed

    // Water Flow Sensor
    private final GpioPinDigitalInput waterFlow

    // Water "Floater" (Circuit Breaker) Sensor
    private final GpioPinDigitalInput waterFloater

    // Water Pump
    private final GpioPinDigitalOutput waterPump

    // Control Button
    private final GpioPinDigitalInput controlButton

    // Water Valves
    private final GpioPinDigitalOutput waterValve1
    private final GpioPinDigitalOutput waterValve2
    private final GpioPinDigitalOutput waterValve3
    private final GpioPinDigitalOutput waterValve4
    private final GpioPinDigitalOutput waterValve5
    private final GpioPinDigitalOutput waterValve6
    private final GpioPinDigitalOutput waterValve7

    // Are we running on a RaspberryPi ?
    private boolean raspberry = false

    // Lock to ensure we only open 1 valve at a time
    private boolean valveLocked = false
    private boolean waterLevelIsSave = false

    // Accessed by ControlButtonJob
    public long controlButtonPressedLastInMillis = 0l
    public int controlButtonPressedTimes = 0


    RaspberryService() {

        String osArch
        try {
            osArch = SystemInfo.getOsArch();
            log.info("RaspberryService() - OS Architecture   :  " + osArch);
        }
        catch(UnsupportedOperationException ex){}

        // Do not try to initialize PI4J and GPIO if running on Desktop (amd64)
        if(osArch == "amd64" || osArch == "x86_64") {
            return
        }

        try {
            gpio = GpioFactory.getInstance();
            raspberry = true
            log.info("RaspberryService() - Initialized OK")
        } catch(Exception ex) {
            log.error("RaspberryService() - Error", ex)
        }


        if(raspberry) {

            powerLed = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, PinState.HIGH);
            powerLed.setShutdownOptions(true, PinState.LOW);

            waterFlow = gpio.provisionDigitalInputPin(RaspiPin.GPIO_04, PinPullResistance.PULL_DOWN);
            waterFlow.setShutdownOptions(true);
            waterFlow.addListener({ GpioPinDigitalStateChangeEvent event ->
                System.out.println(" --> Flow Sensor GPIO Pin State Change: " + event.getPin() + " = " + event.getState());
            } as GpioPinListenerDigital)
 
            waterPump = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_05, PinState.LOW);
            waterPump.setShutdownOptions(true, PinState.LOW);

            controlButton = gpio.provisionDigitalInputPin(RaspiPin.GPIO_06, PinPullResistance.PULL_DOWN);
            controlButton.setShutdownOptions(true);
            controlButton.setDebounce(200);
            controlButton.addListener({ GpioPinDigitalStateChangeEvent event ->
                System.out.println(" --> Control Button GPIO Pin State Change: " + event.getState());
                if(event.state == PinState.HIGH) {
                    controlButtonPressedLastInMillis = System.currentTimeMillis()
                    controlButtonPressedTimes++
                }
            } as GpioPinListenerDigital)

            // TODO: This is just to test input
            waterFloater = gpio.provisionDigitalInputPin(RaspiPin.GPIO_29);
            waterFloater.setShutdownOptions(true);
            waterLevelIsSave = waterFloater.isHigh()
            waterFloater.addListener({ GpioPinDigitalStateChangeEvent event ->
                System.out.println(" --> Water Circuit Breaker Pin State Change: " + event.getState());
                if(event.state == PinState.HIGH) {
                    waterLevelIsSave = true
                } else {
                    waterLevelIsSave = false
                }
            } as GpioPinListenerDigital)

            waterValve1 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_21, RaspberryOutput.output1.toString(), PinState.LOW);
            waterValve1.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

            waterValve2 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_22, RaspberryOutput.output2.toString(), PinState.LOW);
            waterValve2.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

            waterValve3 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_23, RaspberryOutput.output3.toString(), PinState.LOW);
            waterValve3.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

            waterValve4 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_24, RaspberryOutput.output4.toString(), PinState.LOW);
            waterValve4.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

            waterValve5 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_25, RaspberryOutput.output5.toString(), PinState.LOW);
            waterValve5.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

            waterValve6 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_26, RaspberryOutput.output6.toString(), PinState.LOW);
            waterValve6.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

            waterValve7 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_27, RaspberryOutput.output7.toString(), PinState.LOW);
            waterValve7.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);

        }

    }

    @EventListener
    void onShutdown(final ServiceShutdownEvent event) {
        if(raspberry) {
            log.debug("onShutdown() - gpio.shutdown()")
            gpio.shutdown();
        }
    }

    @Synchronized
    protected void doWater(Valve valve) {

        if(raspberry && valveLocked) {
            log.error("doWater() - ${valve.toString()} valve is locked / in use, aborting.")
            eventDataService.save(new Event(valve: valve, message: "Valve Locked - Aborting."))
            return
        }

        if(raspberry && !waterLevelIsSave) {
            log.error("doWater() - ${valve.toString()} water level not safe, aborting.")
            eventDataService.save(new Event(valve: valve, message: "Water Level Low - Aborting."))
            return
        }

        if(raspberry) {

            log.info("doWater() - ${valve.toString()}")

            valveLocked = true

            eventDataService.save(new Event(valve: valve, message: "Begin watering ${valve.name}"))
            valveOpen(valve.output)
            pumpStart()

            // TODO: Measure flow somehow ? Or use quota
            Thread.sleep(valve.quota * 3 * 1000);

            pumpStop()
            valveClose(valve.output)
            eventDataService.save(new Event(valve: valve, message: "End watering ${valve.name}"))

            valveLocked = false

        } else {
            log.info("doWater() - locked or not running on a Raspberry PI")
            Thread.sleep(1 * 1000)
        }

    }

    private void pumpStart() {
        log.debug("pumpStart()")
        waterPump.high()
    }

    private void pumpStop() {
        log.debug("pumpStop()")
        waterPump.low()
    }

    private void valveOpen(RaspberryOutput output) {

        String gpioName = output.toString()
        log.debug("valveOpen() - ${gpioName}")

        GpioPinDigitalOutput pin = (GpioPinDigitalOutput) gpio.getProvisionedPin(gpioName)
        if(pin != null) {
            pin.high()
        }
    }

    private void valveClose(RaspberryOutput output) {

        String gpioName = output.toString()
        log.debug("valveClose() - ${gpioName}")

        GpioPinDigitalOutput pin = (GpioPinDigitalOutput) gpio.getProvisionedPin(gpioName)
        if(pin != null) {
            pin.low()
        }
        Thread.sleep(1000)
    }


    // Only for testing without a button
    void controlButtonPressed() {
        controlButtonPressedLastInMillis = System.currentTimeMillis()
        controlButtonPressedTimes++
    }

}
