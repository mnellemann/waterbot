package waterbot.services

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import waterbot.domain.Valve

import javax.inject.Inject
import javax.inject.Singleton

@Slf4j
@Singleton
@CompileStatic
class EnqueueService {

    @Inject
    QueueService queueDataService

    @Inject
    ValveDataService valveDataService


    def enqueue() {

        List<Valve> valves = valveDataService.list()
        valves.each { Valve valve ->
            log.info("enqueue() - adding ${valve.name} to queue")
            queueDataService.offer(valve)
        }

    }

}
