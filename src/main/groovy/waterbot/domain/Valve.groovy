package waterbot.domain

import grails.gorm.annotation.Entity
import waterbot.RaspberryOutput

@Entity
class Valve {

    String name
    Integer quota
    RaspberryOutput output

    static constraints = {
        name nullable: false, unique: true
        quota min: 1, max: 10
        output unique: true
    }

    static mapping = {
        autoTimestamp true
    }

    String toString() {
        return this.name
    }

    @Override
    public boolean equals(Object o) {
        Valve v = (Valve) o;
        return name.equals(v.name) == 0
    }

}
