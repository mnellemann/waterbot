package waterbot.domain

import grails.gorm.annotation.Entity

@Entity
class Event {

    Valve valve
    String message
    Date dateCreated

    static constraints = {
        message()
    }

    static mapping = {
        sort dateCreated: "desc"
        autoTimestamp true
        valve fetch: 'join'
    }

    String toString() {
        return "${this.valve.name} - ${this.message}"
    }
}
