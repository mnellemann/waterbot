package waterbot.jobs

import groovy.util.logging.Slf4j
import waterbot.domain.Valve
import waterbot.services.QueueService
import waterbot.services.RaspberryService
import waterbot.services.ValveDataService

import javax.inject.Inject
import javax.inject.Singleton
import io.micronaut.scheduling.annotation.Scheduled

@Slf4j
@Singleton
class ControlButtonJob {

    @Inject
    RaspberryService raspberryService

    @Inject
    ValveDataService valveDataService

    @Inject
    QueueService queueDataService

    @Scheduled(fixedRate = "2s")
    void process() {

        if(raspberryService.controlButtonPressedTimes > 0) {

            if ((raspberryService.controlButtonPressedLastInMillis + 2000) < System.currentTimeMillis()) {

                Valve valveInstance = valveDataService.get(raspberryService.controlButtonPressedTimes)
                if (valveInstance == null) {
                    log.warn("controlButtonPressed() - No valve matching controlButtonPressedTimes=" + raspberryService.controlButtonPressedTimes)
                    raspberryService.controlButtonPressedTimes = 0
                    return
                }

                log.debug("process() - Enqueuing valve (${valveInstance.toString()}) due to " + raspberryService.controlButtonPressedTimes + " control button clicks")
                queueDataService.offer(valveInstance)
                raspberryService.controlButtonPressedTimes = 0
            }

       }

   }

}
