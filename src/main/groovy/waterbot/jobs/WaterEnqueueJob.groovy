package waterbot.jobs

import groovy.util.logging.Slf4j
import waterbot.services.EnqueueService
import waterbot.services.ValveDataService

import javax.inject.Inject
import javax.inject.Singleton
import io.micronaut.scheduling.annotation.Scheduled

@Slf4j
@Singleton
class WaterEnqueueJob {

    @Inject
    EnqueueService enqueueService

    @Scheduled(fixedRate = "4h")
    void process() {
        enqueueService.enqueue()
    }
}
