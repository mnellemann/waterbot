package waterbot.jobs

import waterbot.services.DispatchService

import javax.inject.Inject
import javax.inject.Singleton
import io.micronaut.scheduling.annotation.Scheduled

@Singleton
class QueueDispatchJob {

    @Inject
    DispatchService dispatchService

    @Scheduled(fixedRate = "5s")
    void process() {
        dispatchService.processQueue()
    }
}
