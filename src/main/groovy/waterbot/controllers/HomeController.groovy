package waterbot.controllers

import io.micronaut.core.util.CollectionUtils
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.views.View
import waterbot.domain.Event
import waterbot.domain.Valve
import waterbot.services.QueueService
import waterbot.services.RaspberryService
import waterbot.services.ValveDataService
import waterbot.services.EventDataService

import javax.inject.Inject

@Controller("/")
class HomeController {

    @Inject
    ValveDataService valveService

    @Inject
    QueueService queueService

    @Inject
    EventDataService eventService

    @Inject
    RaspberryService raspberryService

    @Get("/")
    HttpResponse index() {
        HttpResponse.redirect(URI.create('/index.html'))
    }

    @View("valveTable")
    @Get("/valveTable")
    public HttpResponse valveTable() {
        List<Valve> list = (List) valveService.list()
        return HttpResponse.ok(CollectionUtils.mapOf("valveList", list))
    }

    @View("queueTable")
    @Get("/queueTable")
    public HttpResponse queueTable() {
        List<Queue> list = (List) queueService.list()
        return HttpResponse.ok(CollectionUtils.mapOf( "queueList", list))
    }

    @View("eventTable")
    @Get("/eventTable")
    public HttpResponse eventTable() {
        List<Event> list = (List) eventService.list()
        return HttpResponse.ok(CollectionUtils.mapOf("eventList", list))
    }

    @Get("/test")
    public HttpResponse test() {
        raspberryService.controlButtonPressed()
        return HttpResponse.ok()
    }
}
