package waterbot.controllers

import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Delete
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.Produces
import io.micronaut.http.annotation.Put
import io.micronaut.http.hateoas.JsonError
import io.reactivex.Single
import groovy.util.logging.Slf4j
import org.grails.datastore.mapping.validation.ValidationException
import waterbot.domain.Valve
import waterbot.services.QueueService
import waterbot.services.ValveDataService
import javax.inject.Inject

@Slf4j
@Controller("/valve")
class ValveController {

    @Inject
    ValveDataService valveService

    @Inject
    QueueService queueService

    @Get("/")
    Single<HttpResponse<?>> list() {
        Single.just(valveService.list()).map({ result ->
            HttpResponse.ok(result)
        }).onErrorReturn({ throwable ->
            new JsonError(throwable.message)
        })
    }

    @Get("/{id}")
    Single<HttpResponse<?>> get(Long id){
        Single.just(valveService.get(id)).map({ result ->
            HttpResponse.ok(result)
        })onErrorReturn({ throwable ->
            new JsonError(throwable.message)
        })
    }

    @Post("/")
    Single<Valve> save(Valve valve){
        Single.just(valveService.save(valve))
    }

    @Put("/")
    Single<Valve> update(Valve valve){
        Valve existingValve = valveService.get(valve.id)
        existingValve.name = valve.name
        existingValve.quota = valve.quota

        Single.just(valveService.save(existingValve))
    }

    @Delete("/")
    Single<HttpResponse<?>> delete(Valve valve){
        Single.just(valve.delete()).map({ result ->
            HttpResponse.ok(result)
        }).onErrorReturn({ throwable ->
            new JsonError(throwable.message)
        })
    }


    @Produces(MediaType.TEXT_HTML)
    @Post(uri="/doWater/{id}", consumes= MediaType.APPLICATION_FORM_URLENCODED)
    public doWater(Long id){

        Valve valve = valveService.get(id)
        try {
            queueService.offer(valve)
        } catch(ValidationException ex) {
            log.warn("valve already present in queue")
        }

        return ""
    }

    @Put(uri="/doUpdate/{id}", consumes= MediaType.APPLICATION_FORM_URLENCODED)
    HttpStatus doUpdate(Long id, String name, Integer quota){
        log.debug("doUpdate()")

        try {
            //queueService.save(new Queue(valve: valve))
        } catch(ValidationException ex) {
            log.warn("valve already present in queue")
        }

        return HttpStatus.OK
    }

}
