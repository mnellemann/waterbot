package waterbot.controllers

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.HttpStatus


@Controller("/event")
class EventController {

    @Get("/")
    HttpStatus index() {
        return HttpStatus.OK
    }
}
