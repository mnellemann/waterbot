package waterbot.controllers

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.HttpStatus


@Controller("/queue")
class QueueController {

    @Get("/")
    HttpStatus index() {
        return HttpStatus.OK
    }
}
