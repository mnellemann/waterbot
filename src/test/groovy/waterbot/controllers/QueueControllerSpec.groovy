package waterbot.controllers

import io.micronaut.http.client.annotation.Client
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import waterbot.services.ValveDataService

import javax.inject.Inject

@MicronautTest(packages="waterbot")
class QueueControllerSpec extends Specification {

    @Inject
    EmbeddedServer embeddedServer

    @Inject
    ValveDataService valveDataService

    @Shared
    @AutoCleanup @Inject @Client("/")
    RxHttpClient client



    void "test index"() {
        given:
        HttpResponse response = client.toBlocking().exchange("/queue")

        expect:
        response.status == HttpStatus.OK
    }
}
