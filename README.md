# Waterbot - Micronaut Version

![Waterbot screenshot](https://bitbucket.org/mnellemann/waterbot/downloads/waterbot.png)


## Building the project

Checkout the code and call the gradle build tasks:

    ./gradlew clean build


## Running the project

### Development

Uses in-memory H2 database and runs on localhost port 8080. The *RaspberryService* will try and detect the RaspberryPi (ARM) platform and will only use the Pi4J features then. This means you can run the code without a RaspberryPi to develop/test user-interface, queue and other parts.  

    ./gradlew run


### Production

Creates a H2 database in the current users home directory.

    java -XX:+ExitOnOutOfMemoryError -Dmicronaut.environments=default,production -jar waterbot-x.y-all.jar

You must run the software as the *pi* user or another user which is a member of the gpio and other related groups.

## RaspberryPi Setup

### Requirements

* Raspberry Pi w. ARMv7 or later
* WiringPi (````sudo apt-get install wiringpi```` and ````sudo reboot````)
* Pi4J 1.2 (download deb file and ````sudo dpkg -i file.deb````)
* JDK 11 (download [Bellsoft Liberica 11](https://download.bell-sw.com/java/11/bellsoft-jdk11-linux-arm32-vfp-hflt.deb) for RaspberryPi and install with ````sudo dpkg -i file.deb ; sudo apt-get -f install````)

### Pin Layout 

Pi4J (by default) uses an abstract [pin numbering scheme](https://pi4j.com/1.2/pin-numbering-scheme.html) to help insulate software from hardware changes.


| GPIO Pin | I/O Direction  | Function / Sensor |       
|----------|----------------|-------------------|
| GPIO_00  | Digital Output | Power LED         |
| GPIO_04  | Digital Input  | Flow Sensor       |
| GPIO_05  | Digital Output | Water Pump        |
| GPIO_06  | Digital Input  | Control Button    |
| GPIO_08  | I2C Bus Data   |                   |
| GPIO_09  | I2C Bus Clock  |                   |
| GPIO_21  | Digital Output | Water Valve 1     |
| GPIO_22  | Digital Output | Water Valve 2     |
| GPIO_23  | Digital Output | Water Valve 3     |
| GPIO_24  | Digital Output | Water Valve 4     |
| GPIO_25  | Digital Output | Water Valve 5     |
| GPIO_26  | Digital Output | Water Valve 6     |
| GPIO_27  | Digital Output | Water Valve 7     |
| GPIO_29  | Digital Input  | Water "Floater"   |


### Components

TODO. More notes here. Schematics, etc.

#### Regulator 5v

LM7805 regulator, husets pakke er TO220 (hvis der er behov for køleplade)


#### Relay Board

Relay skal være active-high ellers tænder det når der ikke er strøm på (GPIO pin HIGH).



#### Water Flow Sensor

*Water Flow Sensor Hall YF-S401*. 
This device should be installed vertically, so fluid flows down through it.

See [this StackExchange answer](https://raspberrypi.stackexchange.com/a/53313) regarding reading the PWM / Hall effect on Raspberry Pi. Suggest converting the PWM to Analog which is easier to read and measure.
